# Contributor: Nicolas Lorin <androw95220@gmail.com>
# Maintainer: Nicolas Lorin <androw95220@gmail.com>
pkgname=crowdsec
pkgver=1.2.3
pkgrel=0
pkgdesc="behavior detection engine, coupled with a global IP reputation network"
url="https://crowdsec.net/"
# riscv64: missing yq, binutils-gold
arch="all !riscv64"
license="MIT"
depends="tzdata yq"
makedepends="go jq bash gettext binutils-gold coreutils"
options="!check" # no test suite identified
source="$pkgname-$pkgver.tar.gz::https://github.com/crowdsecurity/crowdsec/archive/refs/tags/v$pkgver.tar.gz"

build() {
	make BUILD_VERSION=v$pkgver build
}

package() {
	mkdir -p $pkgdir/usr/bin/
	install -m 755 -D ./cmd/crowdsec/crowdsec $pkgdir/usr/bin/crowdsec
	install -m 755 -D ./cmd/crowdsec-cli/cscli $pkgdir/usr/bin/cscli

	mkdir -p $pkgdir/etc/crowdsec/
	mkdir -p $pkgdir/etc/crowdsec/hub/
	install -m 644 -D ./config/config.yaml $pkgdir/etc/crowdsec/
	install -m 644 -D ./config/dev.yaml $pkgdir/etc/crowdsec/
	install -m 644 -D ./config/user.yaml $pkgdir/etc/crowdsec/
	install -m 644 -D ./config/acquis.yaml $pkgdir/etc/crowdsec/
	install -m 644 -D ./config/profiles.yaml $pkgdir/etc/crowdsec/
	install -m 644 -D ./config/simulation.yaml $pkgdir/etc/crowdsec/
}

sha512sums="
3cd03b3c3f7e442b0a032ddb45323e95b104e48d024cefb0b7dc1106db52318063ffa6f8358669f5e0b0b33a8fd704d6110e8f218b594e430e7f11e403d6d571  crowdsec-1.2.3.tar.gz
"
