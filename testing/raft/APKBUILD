# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=raft
pkgver=0.11.3
pkgrel=1
pkgdesc="C implementation of the Raft consensus protocol"
url="https://github.com/canonical/raft"
arch="all"
license="LGPL-3.0-only WITH LGPL-3.0-linking-exception"
makedepends="linux-headers libuv-dev autoconf automake libtool"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/canonical/raft/archive/v$pkgver.tar.gz
	unistd-include.patch"

prepare() {
	default_prepare
	autoreconf -i
}

build() {
	./configure \
		--prefix=/usr \
		--disable-lz4 \
		--enable-example=no
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

sha512sums="
7d1a7455ff8d58ed1f3d259cade1c74fcb22732a55242dcf761cc6554739c0ea1c462aa02591d28d8fa2b343b23e6261a5e15d02e2a31467969b7d9f9f3b5e92  raft-0.11.3.tar.gz
d0f204a271ac40bf9b36027f2a0ee53195aad7366c0166a7f6268bac5bfb05546a5ef858f466c4bbc2a0c6a50dff8d9bc3314abd125f25d49ed5aab9641527b2  unistd-include.patch
"
