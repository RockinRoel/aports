# Contributor: Fabio Ribeiro <fabiorphp@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php81-pecl-redis
_extname=redis
pkgver=5.3.6
_pkgver=${pkgver/_rc/RC}
pkgrel=0
pkgdesc="PHP 8.1 extension for interfacing with Redis - PECL"
url="https://pecl.php.net/package/redis"
arch="all"
license="PHP-3.01"
depends="php81-pecl-igbinary php81-session"
makedepends="php81-dev lz4-dev zstd-dev"
source="php-pecl-$_extname-$_pkgver.tgz::https://pecl.php.net/get/$_extname-$_pkgver.tgz"
builddir="$srcdir/$_extname-$_pkgver"
provides="php81-redis=$pkgver-r$pkgrel" # for backward compatibility
replaces="php81-redis" # for backward compatibility

build() {
	phpize81
	./configure --prefix=/usr --with-php-config=php-config81 \
		--enable-redis-igbinary \
		--enable-redis-lz4 --with-liblz4 \
		--enable-redis-lzf \
		--enable-redis-zstd
	make
}

check() {
	# Need running redis server
	php81 -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install

	local _confdir="$pkgdir"/etc/php81/conf.d
	mkdir -p $_confdir
	echo "extension=$_extname" > $_confdir/20_$_extname.ini
}

sha512sums="
e5047ba9974ff6065be0fe8614aea8bf7d7c77b3a4ca08b19d0bb53069dd164614ffd19bf09bda458511abddce438fd816c28a169536388c8c4cd923596aadf0  php-pecl-redis-5.3.6.tgz
"
