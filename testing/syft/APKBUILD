# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=syft
pkgver=0.36.0
pkgrel=0
pkgdesc="Generate a Software Bill of Materials (SBOM) from container images and filesystems"
url="https://github.com/anchore/syft"
license="Apache-2.0"
arch="all !armhf !armv7 !x86" # FTBFS on 32-bit arches
makedepends="go"
source="https://github.com/anchore/syft/archive/v$pkgver/syft-$pkgver.tar.gz"
options="!check" # tests need docker

export GOFLAGS="$GOFLAGS -trimpath -mod=readonly -modcacherw"
export GOPATH="$srcdir"
export CGO_ENABLED=0

build() {
	go build -o bin/syft
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/syft "$pkgdir"/usr/bin/syft
}

sha512sums="
a572e61a4b1a25c8e296c9d27dbf7b9216abea7646686ea11bcc7fdac5caa12c5b1c2f5e5eeff3db881b796abcb8f7ea4dea69940446fd3c7c81b49f888491e2  syft-0.36.0.tar.gz
"
