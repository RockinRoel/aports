# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>

pkgname=font-iosevka
pkgver=11.2.7
pkgrel=0
pkgdesc="Versatile typeface for code, from code."
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	$pkgname-aile
	$pkgname-etoile
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-etoile-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base
		$pkgname-slab
		$pkgname-curly
		$pkgname-curly-slab
		$pkgname-aile
		$pkgname-etoile
	"

	mkdir -p "$pkgdir"/usr/share/fonts/TTC
	mv "$builddir"/*.ttc "$pkgdir"/usr/share/fonts/TTC
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/TTC/iosevka.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/TTC/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/TTC/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/TTC/iosevka-curly-slab.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/TTC/iosevka-aile.ttc
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	amove usr/share/fonts/TTC/iosevka-etoile.ttc
}

sha512sums="
f58edc961e5e4e70379e1763f4b0f85d66da19ef6a5068fea1f1bd1d5c313aa7dd9d7818eea9ba51300e6492c987218772b55ed00c37d225d82557c5e4cf3927  super-ttc-iosevka-11.2.7.zip
15f2cf70e2e70540f1f05f50a113ee55c7a4c8690ca52e467b3e3d68223537109d7a5ed12772f3b1817f003aeb49ea8952123833a396fc2f7e95dd8da423fb1c  super-ttc-iosevka-slab-11.2.7.zip
c2e33e506afb61e656b8447ba3d6d26eac6f94d10f9792a70165036917d466e5a44f1298c55c0d44318e7b2a3d29837b5b2c717309869f92ff433a38ba90f8f0  super-ttc-iosevka-curly-11.2.7.zip
6242ce180de85f1598a1cb35e41c9aabf70e9890b9d6c066c24273499dade0f5dd5fb24f6fb2dcc0ae238a161dc6dc38dfcf74c280b1126d934010487d7cfff8  super-ttc-iosevka-curly-slab-11.2.7.zip
ad8e31ae96f9e98f1f1ddb837522677b1a9822316377cae834e6bf0d64299110f2803570f22be03ee9b7e8ff8076f58e2a11e4993b520948b369856dddf1e020  super-ttc-iosevka-aile-11.2.7.zip
b2708e2c740ed83aa67e037ad01bfdd77a749f9085ed50ad7ca8b902e4e0be837b14182c64f46461714781fb9839cf30758bc15735180acfff2bc17fd612d60e  super-ttc-iosevka-etoile-11.2.7.zip
"
