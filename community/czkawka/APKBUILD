# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=czkawka
pkgver=4.0.0
pkgrel=0
pkgdesc="Multi functional app to find duplicates, empty folders, similar images etc. (CLI)"
url="https://github.com/qarmin/czkawka"
arch="x86_64 armv7 armhf aarch64 x86 ppc64le"  # limited by rust/cargo
license="MIT"
makedepends="
	alsa-lib-dev
	bzip2-dev
	cargo
	gtk+3.0-dev
	"
subpackages="$pkgname-gui $pkgname-doc"
source="https://github.com/qarmin/czkawka/archive/$pkgver/$pkgname-$pkgver.tar.gz
	minimize-size.patch
	"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -D -m 755 target/release/czkawka_cli "$pkgdir"/usr/bin/czkawka
	ln -s czkawka "$pkgdir"/usr/bin/czkawka_cli

	install -D -m 644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname/
}

gui() {
	pkgdesc="${pkgdesc/CLI/GUI}"
	license="$license AND CC-BY-4.0"

	cd "$builddir"

	install -D -m 755 ./target/release/czkawka_gui -t "$subpkgdir"/usr/bin/

	install -D -m 644 ./data/icons/com.github.qarmin.czkawka.svg \
		-t "$subpkgdir"/usr/share/icons/hicolor/scalable/apps/

	install -D -m 644 ./data/com.github.qarmin.czkawka.desktop \
		-t "$subpkgdir"/usr/share/applications/

	install -D -m 644 ./data/com.github.qarmin.czkawka.metainfo.xml \
		-t "$subpkgdir"/usr/share/metainfo/
}

sha512sums="
628308ec75bdeb36e751137a51d67a6593a3cf0afaeaed8f5a6f819c10f4202701fb3a815db0be287aee3d3e27e6ef6b82f3af6d5e896f4f24e44634d57a977e  czkawka-4.0.0.tar.gz
524cb9331a131c6c44112ca51668c25888b9b9d199b268fc016cfbbe4edd56ce210ab3a928b2972cf8dba881839b8057109cf73bb12929f5051288888e8e36ed  minimize-size.patch
"
