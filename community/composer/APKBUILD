# Contributor: Nathan Johnson <nathan@nathanjohnson.info>
# Maintainer: Dave Hall <skwashd@gmail.com>
pkgname=composer
pkgver=2.2.5
pkgrel=0
pkgdesc="Dependency manager for PHP"
url="https://getcomposer.org/"
arch="noarch"
license="MIT"
_php=php8
depends="$_php $_php-phar $_php-curl $_php-iconv $_php-mbstring $_php-openssl $_php-zip"
checkdepends="git"
options="net"
source="$pkgname-$pkgver.phar::https://getcomposer.org/download/$pkgver/composer.phar"

# secfixes:
#   2.1.9-r0:
#     - CVE-2021-41116
#   2.0.13-r0:
#     - CVE-2021-29472

check() {
	cd "$srcdir"
	$_php $pkgname-$pkgver.phar -Vn
	$_php $pkgname-$pkgver.phar -n diagnose || true # fails as pub-keys are missing
}

package() {
	install -m 0755 -D "$srcdir"/$pkgname-$pkgver.phar "$pkgdir"/usr/bin/$pkgname.phar
	printf "#!/bin/sh\n\n/usr/bin/php8 /usr/bin/composer.phar \"\$@\"\n"\
		> "$pkgdir"/usr/bin/$pkgname
	chmod +x "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
6a18927a413c82c88f19459e5e05eb181678e5ee0692187589803066169ad5fca046732550cf6a53af0af1eb421850e3d8e12c6ca427d64d2e84f17c42b4e621  composer-2.2.5.phar
"
