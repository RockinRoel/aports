# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=rofi-emoji
pkgver=2.2.0
pkgrel=0
pkgdesc="Emoji selector plugin for Rofi"
url="https://github.com/Mange/rofi-emoji"
arch="all !riscv64 !s390x"  # blocked by rofi -> librsvg -> rust
license="MIT"
depends="cmd:rofi"
makedepends="
	autoconf
	automake
	libtool
	rofi-dev
	"
subpackages="
	$pkgname-wayland::noarch
	$pkgname-x11::noarch
	"
source="https://github.com/Mange/rofi-emoji/archive/v$pkgver/rofi-emoji-$pkgver.tar.gz"

prepare() {
	default_prepare

	autoreconf -i
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	cd "$pkgdir"
	rm usr/share/$pkgname/README.md
	rm usr/share/$pkgname/screenshot.png
}

wayland() {
	pkgdesc="$pkgdesc - dependencies for clipboard adapter on Wayland"
	depends="cmd:wl-copy"
	install_if="wayland-libs-server $pkgname=$pkgver-r$pkgrel"

	mkdir -p "$subpkgdir"
}

x11() {
	pkgdesc="$pkgdesc - dependencies for clipboard adapter on X11"
	depends="cmd:xsel"
	install_if="xorg-server $pkgname=$pkgver-r$pkgrel"

	mkdir -p "$subpkgdir"
}

sha512sums="
8fcf363ec8584395707d3230f30268fb6e5a7790d0bf4f7af48cc3026b1512f4a59fec6b87307c4588a23317c60d37f87bf9f58fce167536d43c5500c8780510  rofi-emoji-2.2.0.tar.gz
"
