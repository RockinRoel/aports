# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=ruby-rake-compiler
_gemname=${pkgname#ruby-}
pkgver=1.1.8
pkgrel=0
pkgdesc="Provide a standard and simplified way to build and package Ruby extensions"
url="https://github.com/rake-compiler/rake-compiler"
arch="noarch"
license="MIT"
depends="ruby ruby-rake"
checkdepends="ruby-rspec"
source="$pkgname-$pkgver.tar.gz::https://github.com/rake-compiler/$_gemname/archive/v$pkgver.tar.gz
	gemfile-remove-unwanted-files.patch"
builddir="$srcdir/$_gemname-$pkgver"

build() {
	gem build $_gemname.gemspec
}

check() {
	rspec spec
}

package() {
	local gemdir="$pkgdir/$(ruby -e 'puts Gem.default_dir')"

	gem install --local \
		--install-dir "$gemdir" \
		--bindir "$pkgdir/usr/bin" \
		--ignore-dependencies \
		--no-document \
		--verbose \
		$_gemname

	# Remove unnecessary files and empty directories.
	cd "$gemdir"
	rm -r cache build_info doc
}

sha512sums="
fbb789d1530865f189959a7d5c76ed84502d321fcc22326d5d2652b0841b03f8a2ca2ca229ff4f318c08e960a9a13f2ee6c6780d88f842fff8eab44b3af1754e  ruby-rake-compiler-1.1.8.tar.gz
58e5108f6026db2dafe731a2c39e1da1005070e6207abbec4a4b4b34f05f365a9cd6552a042f41d67d8589b42cebfae67aba1fc0ae6cffa4087bf521ca029e3d  gemfile-remove-unwanted-files.patch
"
